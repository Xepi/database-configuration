# Spesifikasjon av rapportapplikasjon

Applikasjonen skal progammeres som tre C-programmer og et shell-skript. De er beskrevet i hvert sitt avsnitt under.

## rapportapp.sh
Shell-skriptet skal la brukeren velge blant tre rapporter.
1. Programrapport
2. Brukeroversikt
3. Enkeltbruker-info

Brukervalgene leses inn fra standard inngang

**Ved valg 1,** blir brukeren bedt om å spesifisere tidsrom. Deretter skal skriptet starte C-programmet `programrapport` med tidsrom angitt ved hjelp av argumenter.

**Ved valg 2,** blir brukeren bedt om å spesifisere tidsrom. Deretter skal skriptet starte C-programmet `brukeroversikt` med tidsrom angitt ved hjelp av argumenter.

**Ved valg 3**, blir brukeren bedt om å spesifisere bruker Deretter skal skriptet starte C-programmet `enkeltbruker-info` med bruker angitt ved hjelp av et argument.

## programmrapport.c
Programmet skal startes med med kommandolinjeargumenter som spesifisere et tidsrom. Deretter skal det skrive ut en rapport, på standard utgang, slik den er beskrevet i avsnittet *Program-rapport* i `oppdrag.pdf`.

## brukeroversikt.c
Programmet skal startes med med kommandolinjeargumenter som spesifisere et tidsrom. Deretter skal det skrive ut en rapport, på standard utgang, slik den er beskrevet i avsnittet *Brukeroversikt* i `oppdrag.pdf`.


## enkeltbruker-info.c
Programmet skal startes med med kommandolinjeargumenter som spesifisere en bruker. Deretter skal det skrive ut en rapport, på standard utgang, slik den er beskrevet i avsnittet *Enkeltbruker-info* i `oppdrag.pdf`.
