
#include <stdio.h>
#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>

#define RESET   	"\033[0m"
#define BOLDWHITE   	"\033[1m\033[37m"

static int callback( void *NotUsed, int argc, char **argv, char **azColName ) {

	int i;
	for(i=0; i<argc; i++) {
		printf("%-25s", argv[i] ? argv[i] : "NULL");
	}
	printf("\n");

	return 0;
}

int main( int argc, char **argv ) {

	sqlite3 *db;
	char *zErrMsg = 0;
	char sql[1024];
	int rc;

	/* Open database */
	rc = sqlite3_open("../sqlite/database.db", &db);
	if(rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);

		return 1;
	}

	/* database database enabling extensions here yeah yeah */
	rc = sqlite3_enable_load_extension(db, 1);

	/* loading math functions extension */
	rc = sqlite3_load_extension(db, "../lib/libsqlitefunctions.so", 0, &zErrMsg);

	/* Header for output */
	printf(BOLDWHITE "%-25s%-25s%-25s%-25s%-25s\n" RESET, "PROGRAM", "AVG RUNTIME","STDEV RUNETIME", "FREQUENCY", "MAX-USER");
	printf("============================================================================================================\n");

	/* Create SQL statement */
	snprintf(sql, sizeof(sql),
			"SELECT File.FILE_PATH, avg((julianday(Process.STOP) - julianday(Process.START))*86400), stdev((julianday(Process.STOP) - julianday(Process.START))*86400), Process.QUANTITY_EXEC, User.NAME \
			FROM User \
			JOIN Session, Job, Process, File \
			WHERE Process.job_JID = Job.JID \
			AND Session.SID = Job.session_SID \
			AND User.USER_ID = Session.user_USER_ID \
			AND User.USER_ID = File.user_USER_ID \
			AND Process.START >= '%s' \
			AND Process.STOP <= '%s' \
			GROUP BY File.FILE_PATH"
	, argv[1], argv[2]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);

	return 0;
}
