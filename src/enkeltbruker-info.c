
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

#define RESET   	"\033[0m"
#define BOLDWHITE   	"\033[1m\033[37m"

static int callback( void *NotUsed, int argc, char **argv, char **azColName ) {

	int i;
	for(i=0; i<argc; i++) {
		printf(BOLDWHITE "%-25s" RESET "%-25s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");

	return 0;
}

int main( int argc, char **argv ) {

	sqlite3 *db;
	char *zErrMsg = 0;
	char sql[1024];
	int rc;

	/* Open database */
	rc = sqlite3_open("../sqlite/database.db", &db);
	if(rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);

		return 1;
	}
	
	/* database database enabling extensions here oh oh */
	rc = sqlite3_enable_load_extension(db, 1);

	/* loading math functions extension */
	rc = sqlite3_load_extension(db, "../lib/libsqlitefunctions.so", 0, &zErrMsg);

	/* Create SQL statement */
	snprintf(sql, sizeof(sql), 
			"SELECT  User.*, avg(QUANTITY_THREAD) AS 'AVG T/P', avg(QUANTITY_PROCESS) AS 'AVG P/J', avg(QUANTITY_JOBS) AS 'AVG J/S',  stdev(QUANTITY_THREAD) AS 'STDEV T/P', stdev(QUANTITY_JOBS) AS 'STDEV J/S',  stdev(QUANTITY_JOBS) AS 'STDEV J/S' \
			FROM User \
			JOIN Session, Job, Process, Open, File, Thread \
			WHERE NAME = '%s' \
			AND USER_ID = Session.user_USER_ID \
			AND Session.SID = Job.session_SID \
			AND Job.JID = Process.job_JID \
			AND Process.PID = Open.process_PID \
			AND Process.PID = Thread.process_PID \
			GROUP BY User.NAME"
	, argv[1]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	/* Create SQL statement */
	snprintf(sql, sizeof(sql), 
			"SELECT file_FILE_PATH as 'FILEPATH', Session.user_USER_ID AS 'OWNERSHIP ID', QUANTITY_OPEN AS 'OPEN' \
			FROM User \
			JOIN Session, Job, Process, Open, File \
			WHERE NAME = '%s' \
			AND USER_ID = Session.user_USER_ID \
			AND Session.SID = Job.session_SID \
			AND Job.JID = Process.job_JID \
			AND Process.PID = Open.process_PID \
			GROUP BY file_FILE_PATH"
	, argv[1]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);

	return 0;
}
