
#include <stdio.h>
#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>

#define RESET   	"\033[0m"
#define BOLDWHITE   	"\033[1m\033[37m"

static int callback( void *NotUsed, int argc, char **argv, char **azColName ) {

	int i;
	for(i=0; i<argc; i++) {
		printf("%-25s", argv[i] ? argv[i] : "NULL");
	}
	printf("\n");

	return 0;
}


int main( int argc, char **argv ) {

	sqlite3 *db;
	char *zErrMsg = 0;
	char sql[1024];
	int rc;

	/* Open database */
	rc = sqlite3_open("../sqlite/database.db", &db);
	if(rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);

		return 1;
	}

	/* enabling extensions */
	rc = sqlite3_enable_load_extension(db, 1);

	/* loading math function extension */
	rc = sqlite3_load_extension(db, "../lib/libsqlitefunctions.so", 0, &zErrMsg);

	/* Header JOB */
	printf("\n\n");
	printf("SESSION\n");
	printf(BOLDWHITE "%-25s%-25s%-25s%-25s\n" RESET, "NAME", "AVG RUNTIME","STDEV RUNTIME", "QUANTITY");
	printf("========================================================================================\n");

	/* Create SQL statement */
	snprintf(sql, sizeof(sql), 
		"SELECT User.NAME, avg((julianday(Session.STOP) - julianday(Session.START))*86400), stdev((julianday(Session.STOP) - julianday(Session.START))*86400), COUNT(*) \
		FROM User \
		JOIN Session \
		WHERE User.USER_ID = Session.user_USER_ID \
		AND Session.START >= '%s' \
		AND Session.STOP <= '%s' \
		GROUP BY User.NAME", 
	argv[1], argv[2]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	/* Header JOB */
	printf("\n\n");
	printf("JOB\n");
	printf(BOLDWHITE "%-25s%-25s%-25s%-25s\n" RESET, "NAME", "AVG RUNTIME","STDEV RUNTIME", "QUANTITY");
	printf("========================================================================================\n");

	/* Create SQL statement */
	snprintf(sql, sizeof(sql), 
		"SELECT User.NAME, avg((julianday(Job.STOP) - julianday(Job.START))*86400), stdev((julianday(Job.STOP) - julianday(Job.START))*86400), COUNT(*) \
		FROM User \
		JOIN Session, Job \
		WHERE User.USER_ID = Session.user_USER_ID \
		AND Session.SID = Job.session_SID \
		AND Job.START >= '%s' \
		AND Job.STOP <= '%s' \
		GROUP BY User.NAME", 
	argv[1], argv[2]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	/* Header JOB */
	printf("\n\n");
	printf("PROCESS\n");
	printf(BOLDWHITE "%-25s%-25s%-25s%-25s\n" RESET, "NAME", "AVG RUNTIME","STDEV RUNTIME", "QUANTITY");
	printf("========================================================================================\n");

	/* Create SQL statement */
	snprintf(sql, sizeof(sql), 
		"SELECT User.NAME, avg((julianday(Process.STOP) - julianday(Process.START))*86400), stdev((julianday(Process.STOP) - julianday(Process.START))*86400), COUNT(*) \
		FROM User \
		JOIN Session, Job, Process \
		WHERE User.USER_ID = Session.user_USER_ID \
		AND Session.SID = Job.session_SID \
		AND Job.JID = Process.job_JID \
		AND Process.START >= '%s' \
		AND Process.STOP <= '%s' \
		GROUP BY User.NAME", 
	argv[1], argv[2]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	/* Header JOB */
	printf("\n\n");
	printf("THREAD\n");
	printf(BOLDWHITE "%-25s%-25s%-25s%-25s\n" RESET, "NAME", "AVG RUNTIME","STDEV RUNTIME", "QUANTITY");
	printf("========================================================================================\n");

	/* Create SQL statement */
	snprintf(sql, sizeof(sql), 
		"SELECT User.NAME, avg((julianday(Thread.STOP) - julianday(Thread.START))*86400), stdev((julianday(Thread.STOP) - julianday(Thread.START))*86400), COUNT(*) \
		FROM User \
		JOIN Session, Job, Process, Thread \
		WHERE User.USER_ID = Session.user_USER_ID \
		AND Session.SID = Job.session_SID \
		AND Job.JID = Process.job_JID \
		AND Process.PID = Thread.process_PID \
		AND Thread.START >= '%s' \
		AND Thread.STOP <= '%s' \
		GROUP BY User.NAME", 
	argv[1], argv[2]);

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);


	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);

	return 0;
}
