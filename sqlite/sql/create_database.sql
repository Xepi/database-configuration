-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS User (
  USER_ID INT NOT NULL,
  NAME VARCHAR(45) NOT NULL,
  SURNAME VARCHAR(45) NOT NULL,
  USERNAME VARCHAR(45) NOT NULL,
  ADDRESS VARCHAR(16) NULL,
  EMAIL VARCHAR(45) NOT NULL,
  PHONE_NUMBER VARCHAR(16) NULL,
  LAST_LOGIN TIMESTAMP NULL,
  PRIMARY KEY (USER_ID));

-- -----------------------------------------------------
-- Table `mydb`.`session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Session (
  SID INT NOT NULL,
  START TIMESTAMP NOT NULL,
  user_USER_ID INT NOT NULL,
  QUANTITY_JOBS INT NULL,
  STOP TIMESTAMP NULL,
  PRIMARY KEY (SID, START),
  FOREIGN KEY (user_USER_ID)
    REFERENCES User (USER_ID));


-- -----------------------------------------------------
-- Table `mydb`.`job`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Job (
  JID INT NOT NULL,
  START TIMESTAMP NOT NULL,
  session_SID VARCHAR(32) NOT NULL,
  QUANTITY_PROCESS INT NULL,
  STOP TIMESTAMP NULL,
  PRIMARY KEY (JID, START),
  FOREIGN KEY (session_SID)
    REFERENCES Session (SID));


-- -----------------------------------------------------
-- Table `mydb`.`process`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Process (
  PID INT NOT NULL,
  START TIMESTAMP NOT NULL,
  job_JID INT NOT NULL,
  QUANTITY_THREAD INT NULL,
  QUANTITY_EXEC INT NULL,
  QUANTITY_OPEN INT NULL,
  STOP TIMESTAMP NULL,
  PRIMARY KEY (PID, START),
  FOREIGN KEY (job_JID)
    REFERENCES Job (JID));


-- -----------------------------------------------------
-- Table `mydb`.`thread`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Thread (
  TID INT NOT NULL,
  START TIMESTAMP NOT NULL,
  process_PID INT NOT NULL,
  STOP TIMESTAMP NULL,
  PRIMARY KEY (TID, START),
  FOREIGN KEY (process_PID)
    REFERENCES Process (PID));


-- -----------------------------------------------------
-- Table `mydb`.`file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS File (
  FILE_PATH VARCHAR(32) NOT NULL,
  user_USER_ID INT NOT NULL,
  PRIMARY KEY (FILE_PATH),
    FOREIGN KEY (user_USER_ID)
    REFERENCES User (USER_ID));


-- -----------------------------------------------------
-- Table `mydb`.`open`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Open (
  file_FILE_PATH VARCHAR(32) NOT NULL,
  process_PID INT NOT NULL,
  process_START TIMESTAMP NOT NULL,
  PRIMARY KEY (file_FILE_PATH), 
  FOREIGN KEY (file_FILE_PATH)
    REFERENCES File (FILE_PATH),
  FOREIGN KEY (process_PID , process_START)
    REFERENCES Process (PID , START));


-- -----------------------------------------------------
-- Table `mydb`.`execute`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Execute (
  file_FILE_PATH VARCHAR(32) NOT NULL,
  process_PID INT NOT NULL,
  process_START TIMESTAMP NOT NULL,
  PRIMARY KEY (file_FILE_PATH),
  FOREIGN KEY (file_FILE_PATH)
    REFERENCES File (FILE_PATH),
  FOREIGN KEY (process_PID , process_START)
    REFERENCES Process (PID , START));
