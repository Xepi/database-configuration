#!/bin/sh

echo " "
echo "WELCOME TO REPORT APP v0.1!"
echo " "
echo "Feature 1: programrapport"
echo "Feature 2: brukeroversikt"
echo "Feature 3: enkeltbruker-info"
echo " "

echo "Choose report type" 
read choice01

if [ "$choice01" = "1" ]
then
	echo " "
	echo "Please specify time range:"
	echo "Enter START"
	read choice02
	echo "Enter STOP"
	read choice03
	echo " "

	(cd bin && ./programmrapport "$choice02" "$choice03")
fi

if [ "$choice01" = "2" ]
then
	echo " "
	echo "Please specify time range"
	echo "Enter START"
	read choice02
	echo "Enter STOP"
	read choice03
	echo " "

	(cd bin && ./brukeroversikt "$choice02" "$choice03")
fi

if [ "$choice01" = "3" ]
then
	echo " "
	echo "Who are you looking for?"
	read choice02
	echo " "

	(cd bin && ./enkeltbruker-info "$choice02")
fi
