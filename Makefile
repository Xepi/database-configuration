
# PROJECT SETTINGS
#=======================================================#

# Directories
## src
PATH_SRC   	= ./src/
## bin
PATH_BIN   	= ./bin/
FOLDER_BIN 	= mkdir -p bin

# Put your program name(s) here
## Main
TARGET01 	= brukeroversikt
TARGET02 	= enkeltbruker-info
TARGET03 	= programmrapport

# Compiler parameters
CC 		= gcc
CFLAGS 		= -l sqlite3 -Wall -g

# File extension
E		= .c


# END PROJECT SETTINGS
#=======================================================#

# Compile/run functions
all: $(TARGET01) $(TARGET02) $(TARGET03)

$(TARGET01): $(PATH_SRC)$(TARGET01)$(E)
	@echo "make: compiling $(TARGET01)"
	@$(FOLDER_BIN)
	@$(CC) $(CFLAGS) -o $(PATH_BIN)$(TARGET01) $(PATH_SRC)$(TARGET01)$(E)

$(TARGET02): $(PATH_SRC)$(TARGET02)$(E)
	@echo "make: compiling $(TARGET02)"
	@$(FOLDER_BIN)
	@$(CC) $(CFLAGS) -o $(PATH_BIN)$(TARGET02) $(PATH_SRC)$(TARGET02)$(E)

$(TARGET03): $(PATH_SRC)$(TARGET03)$(E)
	@echo "make: compiling $(TARGET03)"
	@$(FOLDER_BIN)
	@$(CC) $(CFLAGS) -o $(PATH_BIN)$(TARGET03) $(PATH_SRC)$(TARGET03)$(E)


.PHONY: clean
clean:
	@echo "make: deleting bin folder"
	@$(RM) -rf $(PATH_BIN)

.PHONY: run
run:
	@./rapportapp.sh
